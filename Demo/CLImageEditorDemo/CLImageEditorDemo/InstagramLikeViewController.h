//
//  InstagramLikeViewController.h
//  PhotoEditor-In-App
//
//  Created by User543 on 06.06.17.
//  Copyright © 2017 CALACULU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

@interface InstagramLikeViewController : UIViewController <
SKProductsRequestDelegate,SKPaymentTransactionObserver>
{
    SKProductsRequest *productsRequest;
    NSArray *validProducts;
}
- (void)fetchAvailableProducts;
- (BOOL)canMakePurchases;
- (void)purchaseMyProduct:(SKProduct*)product;


-(void)get5coins;
-(void)productPurchasedWithID:(NSString*)productIdentifier transaction:(SKPaymentTransaction *)transaction;
@property (strong, nonatomic) NSString *linkProduct;
@end
