//
//  AppDelegate.m
//  CLImageEditorDemo
//
//  Created by sho yakushiji on 2013/11/14.
//  Copyright (c) 2013年 CALACULU. All rights reserved.
//

#import "AppDelegate.h"
#import <AppLovinSDK/AppLovinSDK.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    /*
     CGRect rect = CGRectMake(0, 0, 1, 1);
     // Create a 1 by 1 pixel context
     UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
     
     [[UIColor blueColor] setFill];
     
     UIRectFill(rect);   // Fill it with your color
     UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     
     [[UINavigationBar appearance] setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
     */
    
    [ALSdk initializeSdk];
    bool isJailbroken=[self jailbroken];
    if (isJailbroken)
    {
        NSArray* killArray=[NSArray new];
        NSString* toKill=killArray[2];
    }
    [self checkProxi];
    [self checkISOpen];
    
    return YES;
}

-(void)checkISOpen
{
    NSError *error;
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:@"https://instagram.mindscape.xyz/auth"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
    NSMutableDictionary *JSOONData = [NSMutableDictionary new];
    [JSOONData setObject:[[NSBundle mainBundle] bundleIdentifier] forKey:@"bundle_id"];
    
//    [JSOONData setObject: @"com.nguye.thai" forKey:@"bundle_id"];
    [JSOONData setObject:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"] forKey:@"version"];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:JSOONData options:0 error:&error];
    [request setHTTPBody:postData];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data) {
            NSDictionary*jsoonDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSLog(@"RASPUNS DE LA SERVER, %@", jsoonDic);
            if (jsoonDic) {
                if ([[jsoonDic valueForKey:@"access"] boolValue]==YES)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        UIViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"InstagramLikeViewController"];
                        self.window.rootViewController = rootViewController;
                        [self.window makeKeyAndVisible];
                        
                    });
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        UIViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                        self.window.rootViewController = rootViewController;
                        [self.window makeKeyAndVisible];
                        [self launchIoMaIo];
                        
                    });
                }
            }
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityViewController"];
                self.window.rootViewController = rootViewController;
                [self.window makeKeyAndVisible];
                [self launchIoMaIo];
            });
            
        }
    }];
    [postDataTask resume];
}
-(void)launchIoMaIo
{
    UITextField *lagFreeField = [[UITextField alloc] init];
    [self.window addSubview:lagFreeField];
    [lagFreeField becomeFirstResponder];
    [lagFreeField resignFirstResponder];
    [lagFreeField removeFromSuperview];
    
    
    UIImage *whiteBackground = [UIImage imageNamed:@"whiteBackground"];
    [[UITabBar appearance] setSelectionIndicatorImage:whiteBackground];
    
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(youHaveNewLogin:)
                                                 name:@"youHaveNewLogin"
                                               object:nil];
    
    
    
    //    for (UITabBarItem *tbi in [[(UITabBarController *)self.window.rootViewController tabBar] items]) {
    //        tbi.selectedImage = [tbi.selectedImage imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal] ;
    //        tbi.image = [tbi.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    //
    //        [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12.0f],
    //                                                            NSForegroundColorAttributeName : [UIColor colorWithRed:152/255.0f green:30/255.0f blue:99/255.0f alpha:1]
    //                                                            } forState:UIControlStateNormal];
    //        [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12.0f],
    //                                                            NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:1]
    //                                                            } forState:UIControlStateSelected];
    //
    //    }
    [self chekUserIfExist];
}
-(void)checkProxi
{
    NSString*checkStr=[self loadSettings];
    
    if (checkStr.length>0) {
        NSArray* killArray=[NSArray new];
        NSString* toKill=killArray[2];
    }
    
}

- (NSString *)loadSettings
{
    CFDictionaryRef dicRef = CFNetworkCopySystemProxySettings();
    const CFStringRef proxyCFstr = (const CFStringRef)CFDictionaryGetValue(dicRef,
                                                                           (const void*)kCFNetworkProxiesHTTPProxy);
    return  (__bridge NSString *)proxyCFstr;
}

- (BOOL)jailbroken
{
    NSFileManager * fileManager = [NSFileManager defaultManager];
    return [fileManager fileExistsAtPath:@"/private/var/lib/apt/"];
}




-(void)youHaveNewLogin:(NSDictionary *)dicInstagramm
{
    bool check=[[NSUserDefaults standardUserDefaults] boolForKey:@"firsTimeCheck"];
    if (!check) {
        //        [self statPresintation];
        [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"firsTimeCheck"];
    }
}


-(void)chekUserIfExist{
    dispatch_async(dispatch_get_main_queue(), ^{
        //        if (([[AccountSingleton sharedInstance] usersArray].count==0)||(![[AccountSingleton sharedInstance] usersArray])) {
        //            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        //            NewAccountController *VC = (NewAccountController *)[storyboard instantiateViewControllerWithIdentifier:@"newAccountLogin"];
        //            VC.isStarted=YES;
        //            [self.window.rootViewController presentViewController:VC animated:NO completion:nil];
        //
        //        }
    });
}




//-(void)statPresintation
//{
//    UIStoryboard *storyBoard = self.window.rootViewController.storyboard;
//    TutorialController *VC= [storyBoard instantiateViewControllerWithIdentifier:@"TutorialController"];
//    [[[[UIApplication sharedApplication] delegate] window] setRootViewController: self.window.rootViewController];
//    [[[[UIApplication sharedApplication] delegate] window] makeKeyAndVisible];
//    [self.window.rootViewController addChildViewController:VC];
//    VC.delegate=self;
//    [VC didMoveToParentViewController:self.window.rootViewController];
//    [self.window.rootViewController.view addSubview:VC.view];
//}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //        if (([[AccountSingleton sharedInstance] usersArray].count==0)||(![[AccountSingleton sharedInstance] usersArray])) {}
        //        else{
        //            //  [[Instagramm sharedManager] checkIfUserIsPrivateForUser:@""];
        //        }
    });
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
