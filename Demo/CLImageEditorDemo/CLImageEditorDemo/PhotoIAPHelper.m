//
//  PhotoIAPHelper.m
//  PhotoEditor-In-App
//
//  Created by User543 on 07.06.17.
//  Copyright © 2017 CALACULU. All rights reserved.
//

#import "PhotoIAPHelper.h"

@implementation PhotoIAPHelper

+ (PhotoIAPHelper *)sharedInstance {
    static dispatch_once_t once;
    static PhotoIAPHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      @"testingtestinap",
                                      nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}

@end
