//
//  PhotoEditorProducts.swift
//  PhotoEditor-In-App
//
//  Created by User543 on 31.05.17.
//  Copyright © 2017 CALACULU. All rights reserved.
//

import Foundation

public struct PhotoEditorProducts {
    
    public static let Coin1 = "Tbuy100coins"
    public static let Coin2 = "Tbuy250coins"
    public static let Coin3 = "Tbuy1000coins"
    public static let Coin4 = "Tbuy2000coins"
    public static let Coin5 = "Tbuy5000coins"
    public static let Coin6 = "Tbuy10000coins"
    public static let Coin7 = "Tbuy60000coins"
    
    public static var productIdentifiers: Set<ProductIdentifier> = [PhotoEditorProducts.Coin1,PhotoEditorProducts.Coin2,PhotoEditorProducts.Coin3,PhotoEditorProducts.Coin4,PhotoEditorProducts.Coin5,PhotoEditorProducts.Coin6,PhotoEditorProducts.Coin7]
    public static let store = IAPHelper(productIds: PhotoEditorProducts.productIdentifiers)
}

func resourceNameForProductIdentifier(_ productIdentifier: String) -> String? {
    return productIdentifier.components(separatedBy: ".").last
}

