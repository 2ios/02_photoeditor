//
//  StoreCoinsCell.swift
//  PhotoEditor-In-App
//
//  Created by User543 on 31.05.17.
//  Copyright © 2017 CALACULU. All rights reserved.
//

import UIKit
import StoreKit

class StoreCoinsCell: UITableViewCell {
    static let priceFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        
        formatter.formatterBehavior = .behavior10_4
        formatter.numberStyle = .currency
        
        return formatter
    }()
    
    var buyButtonHandler: ((_ product: SKProduct) -> ())?
    
    var product: SKProduct? {
        didSet {
            guard let product = product else { return }
            
            textLabel?.text = product.localizedTitle
            
//            if PhotoEditorProducts.store.isProductPurchased(product.productIdentifier) {
//                accessoryType = .checkmark
//                accessoryView = nil
//                detailTextLabel?.text = ""
//            } else if IAPHelper.canMakePayments() {
//                StoreCoinsCell.priceFormatter.locale = product.priceLocale
//                detailTextLabel?.text = StoreCoinsCell.priceFormatter.string(from: product.price)
//                
//                accessoryType = .none
//                accessoryView = self.newBuyButton()
//            } else {
//                detailTextLabel?.text = "Not available"
//            }
            
            if IAPHelper.canMakePayments() {
                StoreCoinsCell.priceFormatter.locale = product.priceLocale
                detailTextLabel?.text = StoreCoinsCell.priceFormatter.string(from: product.price)
                
                accessoryType = .none
                accessoryView = self.newBuyButton()
            } else {
                detailTextLabel?.text = "Not available"
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        textLabel?.text = ""
        detailTextLabel?.text = ""
        accessoryView = nil
    }
    
    func newBuyButton() -> UIButton {
        let button = UIButton(type: .system)
        button.setTitleColor(tintColor, for: UIControlState())
        button.setTitle("Buy", for: UIControlState())
        button.addTarget(self, action: #selector(StoreCoinsCell.buyButtonTapped(_:)), for: .touchUpInside)
        button.sizeToFit()
        
        return button
    }
    
    func buyButtonTapped(_ sender: AnyObject) {
        buyButtonHandler?(product!)
    }

}
