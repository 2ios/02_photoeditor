//
//  TestStoreCoinsTableViewController.swift
//  PhotoEditor-In-App
//
//  Created by User543 on 01.06.17.
//  Copyright © 2017 CALACULU. All rights reserved.
//

import UIKit

class TestStoreCoinsTableViewController: UITableViewController {
    
//    let titleArray: Array<String> = ["50 Coins", "100 Coins", "150 Coins", "200 Coins", "250 Coins"]
//    let moneyArray: Array<String> = ["0,99 $", "1,99 $", "2,99 $", "3,99 $", "4,99 $"]
//    let coinsArray: Array<Int> = [50, 100, 150, 200, 250]
//    
//    @IBAction func cancelBarButton(_ sender: UIBarButtonItem) {
//        dismiss(animated: true, completion: nil)
//    }
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        // Uncomment the following line to preserve selection between presentations
//        // self.clearsSelectionOnViewWillAppear = false
//
//        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
//        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//    // MARK: - Table view data source
//
//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 1
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return 5
//    }
//
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "TestStoreCoinsCell", for: indexPath) as! TestStoreCoinsCell
//
//        // Configure the cell...
//        cell.title.text = "\(titleArray[indexPath.row]) ______________________ \(moneyArray[indexPath.row])"
//        cell.buyButton.tag = indexPath.row
//        cell.buyButton.addTarget(self, action: #selector(buyButtonIsPressed), for: .touchUpInside)
//        return cell
//    }
//
//    func buyButtonIsPressed(_ sender: UIButton) {
//        createAlert(title: titleArray[sender.tag], message: moneyArray[sender.tag], tag: sender.tag)
//    }
//    
//    func createAlert(title: String, message: String, tag: Int) {
//        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        
//        alert.addAction(UIAlertAction(title: "Buy", style: .default, handler: { (action) in
//            UserDefaults.standard.set(self.coinsArray[tag], forKey: "Coins")
//            UserDefaults.standard.synchronize()
//            let temp = UserDefaults.standard.integer(forKey: "Wallet")
//            UserDefaults.standard.set(self.coinsArray[tag]+temp, forKey: "Wallet")
//            UserDefaults.standard.synchronize()
//            alert.dismiss(animated: true, completion: nil)
//        }))
//        
//        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
//            alert.dismiss(animated: true, completion: nil)
//        }))
//
//        self.present(alert, animated: true, completion: nil)
//    }
//    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        print(UserDefaults.standard.integer(forKey: "Wallet"))
//    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
