//
//  IAPHelper.swift
//  PhotoEditor-In-App
//
//  Created by User543 on 31.05.17.
//  Copyright © 2017 CALACULU. All rights reserved.
//

import StoreKit

public typealias ProductIdentifier = String
public typealias ProductsRequestCompletionHandler = (_ success: Bool, _ products: [SKProduct]?) -> ()

open class IAPHelper : NSObject  {
    
    static let IAPHelperPurchaseNotification = "IAPHelperPurchaseNotification"
    fileprivate var productIdentifiers: Set<ProductIdentifier>
    fileprivate var purchasedProductIdentifiers = Set<ProductIdentifier>()
    fileprivate var productsRequest: SKProductsRequest?
    fileprivate var productsRequestCompletionHandler: ProductsRequestCompletionHandler?
    
    public init(productIds: Set<ProductIdentifier>) {
        productIdentifiers = productIds
        for productIdentifier in productIds {
//            let purchased = UserDefaults.standard.bool(forKey: productIdentifier)
//            if purchased {
//                purchasedProductIdentifiers.insert(productIdentifier)
//                print("Previously purchased: \(productIdentifier)")
//            } else {
//                print("Not purchased: \(productIdentifier)")
//            }
            print("Not purchased: \(productIdentifier)")
        }
        super.init()
        SKPaymentQueue.default().add(self)
    }
    
}

// MARK: - StoreKit API

extension IAPHelper {
    
    public func requestProducts(completionHandler: @escaping ProductsRequestCompletionHandler) {
        productsRequest?.cancel()
        productsRequestCompletionHandler = completionHandler
        
//        let buyVC: InstagramLikeViewController = UIApplication.shared.keyWindow?.rootViewController as! InstagramLikeViewController
//        productIdentifiers = [buyVC.linkProduct!]
        
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        productsRequest!.delegate = self
        productsRequest!.start()
    }
    
    public func buyProduct(_ product: SKProduct) {
        print("Buying \(product.productIdentifier)...")
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
    
    public func isProductPurchased(_ productIdentifier: ProductIdentifier) -> Bool {
        return purchasedProductIdentifiers.contains(productIdentifier)
    }
    
    public class func canMakePayments() -> Bool {
        return SKPaymentQueue.canMakePayments()
    }
    
//    public func restorePurchases() {
//        SKPaymentQueue.default().restoreCompletedTransactions()
//    }
}

// MARK: - SKProductsRequestDelegate

extension IAPHelper: SKProductsRequestDelegate {
    
    public func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        let products = response.products
        print("Loaded list of products...")
        productsRequestCompletionHandler?(true, products)
        clearRequestAndHandler()
        
        for p in products {
            print("Found product: \(p.productIdentifier) \(p.localizedTitle) \(p.price.floatValue)")
        }
    }
    
    public func request(_ request: SKRequest, didFailWithError error: Error) {
        print("Failed to load list of products.")
        print("Error: \(error.localizedDescription)")
        productsRequestCompletionHandler?(false, nil)
        clearRequestAndHandler()
    }

    private func clearRequestAndHandler() {
        productsRequest = nil
        productsRequestCompletionHandler = nil
    }
}

// MARK: - SKPaymentTransactionObserver

extension IAPHelper: SKPaymentTransactionObserver {
    
    public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch (transaction.transactionState) {
            case .purchased:
                complete(transaction: transaction)
                break
            case .failed:
                fail(transaction: transaction)
                break
            case .restored:
//                restore(transaction: transaction)
                break
            case .deferred:
                break
            case .purchasing:
                break
            }
        }
    }
    
    private func complete(transaction: SKPaymentTransaction) {
        print("complete...")
        
//        let VC: InstagramLikeViewController = UIApplication.shared.keyWindow?.rootViewController as! InstagramLikeViewController
//        VC.productPurchased(withID: transaction.transactionIdentifier!, transaction: transaction)
        
        deliverPurchaseNotificationFor(identifier: transaction.payment.productIdentifier)
        
        SKPaymentQueue.default().finishTransaction(transaction)
        var result = UserDefaults.standard.integer(forKey: "Wallet")
        
        if transaction.payment.productIdentifier == "Tbuy100coins" {
            result = result + 100
        } else if transaction.payment.productIdentifier == "Tbuy250coins" {
            result = result + 250
        } else if transaction.payment.productIdentifier == "Tbuy1000coins" {
            result = result + 1000
        } else if transaction.payment.productIdentifier == "Tbuy2000coins" {
            result = result + 2000
        } else if transaction.payment.productIdentifier == "Tbuy5000coins" {
            result = result + 5000
        } else if transaction.payment.productIdentifier == "Tbuy10000coins" {
            result = result + 10000
        } else if transaction.payment.productIdentifier == "Tbuy60000coins" {
            result = result + 60000
        } else {
            result = result + 0
        }
        UserDefaults.standard.set(result, forKey: "Wallet")
        UserDefaults.standard.synchronize()
    }
    
//    private func restore(transaction: SKPaymentTransaction) {
//        guard let productIdentifier = transaction.original?.payment.productIdentifier else { return }
//        
//        print("restore... \(productIdentifier)")
//        deliverPurchaseNotificationFor(identifier: productIdentifier)
//        SKPaymentQueue.default().finishTransaction(transaction)
//    }
    
    private func fail(transaction: SKPaymentTransaction) {
        print("fail...")
        if let transactionError = transaction.error as NSError? {
            if transactionError.code != SKError.paymentCancelled.rawValue {
                print("Transaction Error: \(String(describing: transaction.error?.localizedDescription))")
            }
        }
        
        SKPaymentQueue.default().finishTransaction(transaction)
    }
    
    private func deliverPurchaseNotificationFor(identifier: String?) {
        guard let identifier = identifier else { return }
        
        purchasedProductIdentifiers.insert(identifier)
        UserDefaults.standard.set(true, forKey: identifier)
        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseNotification), object: identifier)
    }
}

