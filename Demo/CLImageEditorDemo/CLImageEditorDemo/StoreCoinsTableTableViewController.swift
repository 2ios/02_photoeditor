//
//  StoreCoinsTableTableViewController.swift
//  PhotoEditor-In-App
//
//  Created by User543 on 31.05.17.
//  Copyright © 2017 CALACULU. All rights reserved.
//

import UIKit
import StoreKit

class StoreCoinsTableViewController: UITableViewController {
    
    @IBAction func cancelBarButton(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    var products = [SKProduct]()
    var watchVideo = [String]()
    var alert: UIAlertController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Store coins"
        
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(StoreCoinsTableViewController.reload), for: .valueChanged)
        
//        let restoreButton = UIBarButtonItem(title: "Restore",
//                                            style: .plain,
//                                            target: self,
//                                            action: #selector(StoreCoinsTableViewController.restoreTapped(_:)))
//        navigationItem.rightBarButtonItem = restoreButton
        
//        NotificationCenter.default.addObserver(self, selector: #selector(StoreCoinsTableViewController.handlePurchaseNotification(_:)),
//                                               name: NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseNotification),
//                                               object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showWaiting {
            self.reload()
        }
        
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
        noDataLabel.text = ""
        noDataLabel.textColor = UIColor.black
        noDataLabel.textAlignment = .center
        tableView.backgroundView = noDataLabel
    }
    
    func reload() {
        products = []
        
        tableView.reloadData()
        
        PhotoEditorProducts.store.requestProducts{success, products in
            if success {
                self.products = products!
                self.watchVideo = ["Get 5 coins"]
                
                self.tableView.reloadData()
                self.check()
            } else {
                self.check()
                
                let alert = UIAlertController(title: "", message: "Failed to load list of products.", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .cancel) { (action) in
                    self.dismiss(animated: true, completion: nil)
                }
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            self.refreshControl?.endRefreshing()
        }
    }
    
    func showWaiting(completion: @escaping () -> ()) -> Void {
        if alert == nil {
            alert = UIAlertController(title: "", message: "Loading\n\n\n", preferredStyle: .alert)
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            spinner.center = CGPoint(x: 135.0, y: 65.5)
            spinner.color = UIColor.black
            spinner.startAnimating()
            alert?.view.addSubview(spinner)
        }
        
        present(alert!, animated: true, completion: completion)
    }
    
    func hideWaiting() -> Void {
        alert?.dismiss(animated: true, completion: nil)
    }
    
    func check()->Void {
        tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        tableView.backgroundView = nil
        hideWaiting()
    }
//    func restoreTapped(_ sender: AnyObject) {
//        PhotoEditorProducts.store.restorePurchases()
//    }
//    
//    func handlePurchaseNotification(_ notification: Notification) {
//        guard let productID = notification.object as? String else { return }
//        
//        for (index, product) in products.enumerated() {
//            guard product.productIdentifier == productID else { continue }
//            
//            tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .fade)
//        }
//    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return watchVideo.count
        } else {
            return products.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let freeCell = tableView.dequeueReusableCell(withIdentifier: "FreeCoinsCell", for: indexPath) as! StoreCoinsCell
            freeCell.textLabel?.text = watchVideo[indexPath.row]
            freeCell.accessoryView = self.newWatchButton()
            return freeCell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "StoreCoinsCell", for: indexPath) as! StoreCoinsCell
            let product = products[(indexPath as NSIndexPath).row]
            cell.product = product
            cell.buyButtonHandler = { product in
                PhotoEditorProducts.store.buyProduct(product)
            }
            return cell
        }
    }
    
    func newWatchButton() -> UIButton {
        let button = UIButton(type: .system)
        button.setTitle("Watch video", for: UIControlState())
        button.addTarget(self, action: #selector(watchButtonTapped(_:)), for: .touchUpInside)
        button.sizeToFit()
        
        return button
    }
    
    func watchButtonTapped(_ sender: AnyObject) {
        
        print("Reclama activa")
        
        let test = self.storyboard?.instantiateViewController(withIdentifier: "Test")
        self.navigationController?.pushViewController(test!, animated: true)
    }

}

